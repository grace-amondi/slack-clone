export default (sequelize, DataTypes) => {
    const Message = sequelize.define('message', {
        text: DataTypes.STRING,
    });

    Message.associate = function (models) {
        //M : M
        Message.belongsToMany(models.User, {
            through: 'member',
            foreignKey: {
                name: 'channelId',
                field: 'channel_id'
            },
        });
        //1 : M
        Message.belongsTo(models.User, {
            foreignKey: {
                name: 'userId',
                field: 'user_id'
            }
        });
    };

    return Message;
};