module.exports = {
    "extends": "airbnb",
    "plugins":[
        "react",
        "jsx-ally",
        "import"
    ],
    rules:{
        "react/jsx-filemame-extension":0
    },
    "globals":{
        "document":1
    },
};